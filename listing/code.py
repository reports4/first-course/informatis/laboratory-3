arr = []
for line in open('Data.txt', 'r'):
    spl = line.split()
    name = spl[0] + ' ' + spl[1]
    day = spl[2]
    avsum = (int(spl[3]) + int(spl[4]) + int(spl[5])) / 3.0
    ball = spl[3] + ' ' + spl[4] + ' ' + spl[5]
    arr.append([name, day, ball, avsum])

arr = sorted(arr, key=lambda x: x[3], reverse=True)

for line in arr:
    out = line[0] + " | " + line[1] + " | " + line[2] + " -> " + str(line[3])
    print(out)